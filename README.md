# README #

## Sobre

Este repositório consiste no compartilhamento de códigos que poderão ser utilizados pelos desenvolvedores na manutenção dos ambientes da AWS. Na versão inicial estamos armazenando scripts lambda para ligar e desligar instâncias, bem como anexá-las e removê-las de Load Balancer dos tipos "Classic" e "Application". Também estão neste repositório as IAM Roles necessárias para a execução destes scripts na AWS.   

Posteriormente seu conteúdo poderá ser incrementado com novos scripts.   


## Pré-requisitos e utilização dos scripts   

Dividimos essa seção entre scripts para gestão de instâncias sem Load Balancer, com Classic Load Balancer e Application Load Balancer   

### Scripts para gestão sem Load Balancer   

#### Pré-requisito para uso   
As instâncias afetadas pelo script deverão ter a seguinte tag:   
>  **ScheduledStartStop: True**
	
#### Instruções de uso   
 - **Arquivo com regras específicas para IAM Role:** iam_roles.json   
 - **Função para start de instância:** start_instances.py   
 - **Função para stop de instância:** stop_instances.py   
